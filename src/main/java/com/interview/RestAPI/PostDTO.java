package com.interview.RestAPI;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class PostDTO {
    public PostDTO() {

    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    private int userId;
    private String title;
}
