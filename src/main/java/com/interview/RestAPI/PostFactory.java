package com.interview.RestAPI;

public class PostFactory {
    public static PostDTO createPost(int userId, String title) {
        PostDTO post = new PostDTO();
        post.setUserId(userId);
        post.setTitle(title);
        return post;
    }
}
