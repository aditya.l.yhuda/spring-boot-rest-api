package com.interview.RestAPI;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/posts")
public class PostController {
    @GetMapping
    public ResponseEntity<?> getPosts(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") int size) {

        if (page < 0 || size <= 0) {
            throw new InvalidPageRequestException("Page and size must be positive integers.");
        }

        String url = "https://jsonplaceholder.typicode.com/posts";
        WebClient webClient = WebClient.create(url);

        List<PostDTO> posts = webClient.get()
                .retrieve()
                .bodyToFlux(JsonNode.class)
                .toStream()
                .map(jsonNode -> PostFactory.createPost(
                        jsonNode.get("userId").asInt(),
                        jsonNode.get("title").asText()))
                .collect(Collectors.toList());

        int start = Math.min(page * size, posts.size());
        int end = Math.min(start + size, posts.size());

        List<PostDTO> pagedPosts = posts.subList(start, end);
        return ResponseEntity.ok(pagedPosts);
    }
}
